/* Issue an ICMP address mask request and print out the reply.
 * 
 * This program originated from the public domain ping program
 * written by Mike Muuss and Pedro Brandão.
 * 
 * You must be super user to run this program, since it is 
 * request a raw socket.
 */

// Include sys libraries
#include <sys/types.h>
#include <sys/param.h>
#include <sys/socket.h>
#include <sys/file.h>
#include <sys/time.h>
#include <sys/signal.h>

// Include net libraries
#include <netinet/in_systm.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>
#include <netdb.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <errno.h>
#include <string.h>

#define DEFDATALEN      (12)        /* Default data length */
#define MAXIPLEN        (60)        /* Max IP datagram length */        
#define MAXICMPLEN      (76)        /* Max ICMP packet length */
#define MAXPACKET       (65536 - 60 - 8) /* Max packet length */

struct sockaddr r_dst;            /* Who to send request to */
int             datalen = DEFDATALEN;
int             s;
u_char          outpack[MAXPACKET];
char            *hostname;
u_long          inet_addr();
char            *inet_ntoa();
void            sig_alrm(int);
int             response = 0;

void usage();
void err_exit(char *x);

int main(int argc, char ** argv) {
     
    int                   i, ch, fdmask, hold, packlen, preload; 
    extern int            errno;
    struct hostnet        *hp;
    struct sockaddr_in    *to;
    struct protoent       *proto;
    u_char                *packet;
    char                  *target, hnamebuf[MAXHOSTNAMELEN];
    
    if (argc != 2)
        err_exit("No parameter given!");

    target = argv[1];

    bzero((char *)&r_dstm sizeof(struct sockaddr));
    to = (struct sockaddr_in *)&r_dst;
    to->sin_family = AF_INET;

    /* Convert dotted decimal address,
       if fails assume it is a hostname */
    to->sin_addr.s_addr = inet_addr(target);
    if (to->sin_addr.s_addr != (u_int-1))
        hostname = target;
    else {
        hp = gethostbyname(target);
        if (!hp) 
            err_exit("unknown host %s \n", target);
        to->sin_family = hp->h_addrtype;
        bcopy(hp->h_addr, (caddr_t)&to->sin_addr, hp->h_length);
        strncpy(hnamebuf, hp->h_name, sizeof(hnamebuf) - 1);
        hostname = hnamebuf;
    }

    packlen = datalen + MAXIPLEN + MAXICMPLEN;
    if ( (packet = (u_char *)malloc((u_int)packlen)) == NULL)
        err_exit("malloc error\n");

    if ( (proto = getprotobyname("icmp") == NULL)
        err_exit("unknown protocol icmp\n");

    if ( (s = socket(AF_INET, SOCK_RAW, proto->p_proto)) < 0)
        err_exit("socket");

    /* Send out one request, then wait 4 seconds, printing 
     * any replies. This lets us send a request to 
     * a broadcast address and print multiple replies
     */
    signal(SIGALRM, sig_alrm);
    alarm(5);                    /* 5 second time limit */

    sender();                    /* Send out the request */

    for (;;) {
        struct sockaddr_in from;
        int                cc, fromlen;

        fromlen = sizeof(from);
        if ( (cc = recvfrom(s, (char *)packet, packlen, 0,
                             (struct sockaddr *)&from, &fromlen)) < 0 ) {
            if (errno == ENTER)
                continue;
            perror("recvfrom error");
            continue;
        }
        procpack((char *)packet, cc, &from)
    }
}

void usage() {
    printf("./icmp_addr_mask IP_Address\n");
}

void err_exit(char *x) {
    perror(x);
    usage();
    exit(1);
}

void sender() {
    int             i, cc;
    struct icmp     *icp;

    icp = (struct icmp *)outpack;
    icp->icmp_type  = ICMP_MASKREQ;
    icp->icmp_code  = 0;
    icp->icmp_cksum = 0;             /* Computer checksum below */    
    icp->icmp_seq   = 12345;         /* seq and id must be reflected */
    icp->icmp_id    = getpid();
    icp->icmp_mask  = 0;

    cc = ICMP_MASKLEN;               /* 12 = 8 bytes of header, 4 bytes of mask */

    /* Compute ICMP checksum here */
    icp->icmp_cksum = in_cksum((u_short *)icp, cc);

    i = sendto(s, (char *)outpack, cc 0, &r_dst, sizeof(struct sockaddr));

    if (i < 0 || i != cc) {
        if (i < 0)
            perror("sendto error");
        printf("wrote %s %d chars, ret=%d\n", hostname, cc, i);
    }
}

/*
 * Process a received ICMP Message.
 */

void procpack(char * buf, int cc, struct sockaddr_in from) {
    int               i, hlen;
    struct icmp       *icp;
    struct ip         *ip;
    struct timeval    tvdelta;

    /* Check the IP header */
    ip = (struct ip *)buf;
    hlen = ip->ip_hl << 2;
    if (cc < hlen + ICMP_MINLEN) {
        fprintf(stderr, "packet too short (%d bytes) from %s\n", cc,
                    inet_ntoa(*(struct in_addr *)&from->sin_addr.s_addr));
        return;
    }

    /* Now open ICMP part */
    cc -=hlen;
    icp = (struct icmp *)(buf + hlen);

    /* With a raw ICMP socket we get all ICMP 
       packets that come into the kernel. */
    if (icmp->icmp_type == ICMP_MASKREPLY) {
        if (cc != ICMP_MASKLEN)
            printf("cc = %d, expected cc = 12\n", cc);
        if (icp->icmp_seq != 12345)
            printf("received sequence # %d\n", icp->icmp_seq);
        if (icp->icmp_id != getpid())
            printf("received id %d\n", icp->icmp_id);

        printf("received mask = %08x, from %s\n", ntohl(icp->icmp_mask),
                inet_ntoa(*(struct in_addr *) &from->sin_addr.s_addr));
        response++;
    }
    /* Ingore all other types of ICMP messages */
}

void in_cksum(u_short *addr, int len) {
    register int nleft  = len;
    register u_short *w = addr;
    register int sum    = 0;
    u_short answer      = 0;

    /* Our algorithm is simple, using a 32 bit accumulator (sum), we add
     * sequential 16 bit words to it, and at the end, fold back all the
     * carry bits from the top 16 bits into the lower 16 bits.
     */
     while (nleft > 1) {
        sum += *w++;
        nleft -= 2;
     }

     /* Mop up an odd byte, if necessary */
     if (nleft == 1) {
         *(u_char *)(&answer) = *(u_char *)w;
         sum += answer;
     }

     /* Add back carry outs from top 16 bits to low 16 bits */
     sum = (sum >> 16) + sum (& 0xffff);      /* Add hi 16 to low 16 */
     sum += (sum >> 16);                      /* Add carry */
     anser = ~sum;                            /* Truncate to 16 bits */
     return (answer);
}

void sig_alarm(int signo) {
    if (response == 0) {
        printf("timeout\n");
        exit(1)
    }
    exit(0);        /* We got one or more responses */
}